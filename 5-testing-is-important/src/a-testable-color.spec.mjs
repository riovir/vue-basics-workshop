import tap from 'tap';
import Vue from 'vue/dist/vue.common';
import { AColor } from './a-testable-color.mjs';

tap.test('a-testable-color', async t => {
  t.equals(mount({ value: 'green' }).color, 'green', 'uses value as the color');
  t.equals(mount({ randomColor: 'red' }).color, 'red', 'picks random color as fallback');

  function mount({ value, randomColor = 'gray' }) {
    const component = AColor({ getRandomColor: () => randomColor });
    const Constructor = Vue.extend(component);
    const propsData = { value };
    return new Constructor({ propsData }).$mount();
  }
});
