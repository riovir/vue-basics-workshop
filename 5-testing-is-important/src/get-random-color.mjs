export default GetRandomColor();
export { GetRandomColor };

function GetRandomColor({ random = Math.random } = {}) {
  return function getRandomColor() {
    const hexChars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    const randomIndex = () => Math.floor(random() * hexChars.length);
    const randomChar = () => hexChars[randomIndex()];
    return `#${randomChar()}${randomChar()}${randomChar()}`;
  }
}
