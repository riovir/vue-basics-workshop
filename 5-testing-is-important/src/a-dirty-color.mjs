export default {
  template: `
  <div class="box is-inline-flex">
    <div :style="style" />
  </div>`,
  props: {
    value: { type: String }
  },
  computed: {
    color() {
      return this.value || getRandomColor();
    },
    style() {
      return {
        width: '64px',
        height: '64px',
        backgroundColor: this.color
      };
    }
  }
};

function getRandomColor() {
  const hexChars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
  const randomIndex = () => Math.floor(Math.random() * hexChars.length);
  const randomChar = () => hexChars[randomIndex()];
  return `#${randomChar()}${randomChar()}${randomChar()}`;
}
