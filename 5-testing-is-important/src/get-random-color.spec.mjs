import tap from 'tap';
import { GetRandomColor } from './get-random-color.mjs';

tap.test('get-random-color', async t => {
  t.equals(getColorOf(0), '#000', 'can include black');
  t.equals(getColorOf(0.999), '#fff', 'can include white');
  t.equals(getColorOf(0.5), '#888', 'picks color value proportionate of the random value');
  t.equals(getColorWith(0, 0.999, 0), '#0f0', 'calls random for each color');

  function getColorOf(value) {
    const getRandomColor = GetRandomColor({ random: () => value });
    return getRandomColor();
  }

  function getColorWith(...values) {
    let index = -1;
    const getRandomColor = GetRandomColor({ random: () => {
      index++;
      return values[index];
    } });
    return getRandomColor();
  }
});
