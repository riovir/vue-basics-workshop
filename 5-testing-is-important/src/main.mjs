// import AColor from './a-dirty-color.mjs';
import AColor from './a-testable-color.mjs';

new Vue({
  el: '#app',
  template: `
    <div class="section">
      <div class="container">
        <h1 class="title has-text-info">Now with unit tests</h1>

        <a-color value="red" />
        <a-color value="green" />
        <a-color value="#060606" />
        <a-color />

        <p>
          You can get a sneak peek to the future node.js support for native ES6 modules implemented with the
          <a href="https://medium.com/dailyjs/es6-modules-node-js-and-the-michael-jackson-solution-828dc244b8b" target="_blank">
            Michael Jackson solution
          </a>.
          Type <code>npm install</code> then <code>npm test</code> in the <code>/5-testing-is-important</code> directory.
          <strong class="has-text-danger">Warning: this feature is currently in experimental stage.</strong>
        </p>
      </div>
    </div>`,
  components: { AColor }
});
