
import _getRandomColor from './get-random-color.mjs';

export default AColor();
export { AColor };

function AColor({ getRandomColor = _getRandomColor } = {}) {
  return {
    template: `
    <div class="box is-inline-flex">
      <div :style="style" />
    </div>`,
    props: {
      value: { type: String }
    },
    computed: {
      color() {
        return this.value || getRandomColor();
      },
      style() {
        return {
          width: '64px',
          height: '64px',
          backgroundColor: this.color
        };
      }
    }
  }
}
