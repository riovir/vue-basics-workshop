import tap from 'tap';
import Vue from 'vue/dist/vue.common';
import component from './a-dirty-color.mjs';

tap.test('a-dirty-color', async t => {
  t.equals(mount({ value: 'green' }).color, 'green', 'uses value as the color');
  t.equals(mount({}).color, '??!', 'is untestable with the Math.random burned in');

  function mount({ value, randomColor = 'gray' }) {
    const Constructor = Vue.extend(component);
    const propsData = { value };
    return new Constructor({ propsData }).$mount();
  }
});
