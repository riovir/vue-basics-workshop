import 'jsdom-global/register';
import Vue from 'vue/dist/vue.common';
Vue.config.productionTip = false;

import '../src/a-dirty-color.spec.mjs';
import '../src/a-testable-color.spec.mjs';
import '../src/get-random-color.spec.mjs';

