# Vue-basics-workshop

## First things fist

Check if you have everything you need. If not, install it.

```bash
# You'll need git to clone this repo
git --version
# You'll need Node.js (v8.11.x+) and NPM (v5.x+)
npm version
```

## Getting started

Grab the code and open `vue-basics-workshop/1-welcome/index.html` with `Chrome`.

```bash
git clone https://gitlab.com/riovir/vue-basics-workshop.git
cd vue-basics-workshop
```

## Do it yourself

### 1. After **Step 2**
  * Delete the contents of `<body>` in `/2-using-a-cdn/index.html`.
  * On [Bulma](https://bulma.io)'s site find the documentation of `Typography helpers`, `Section` and `Content`.
  * Now create a semi-decent looking list of todo items where the second entry's style indicates `success`.

### 2. After **Step 3**
  * Grab the HTML of your todo list and put it inside the `/3-hello-vue/main.js`.
  * Spend a minute to appreciate the power of `copy & paste`.

### 3. After **Step 4**
  * Implement a Vue component called the `todo-item`. It has a single `prop`: done. If it's true it has to have the class of `has-text-success`. It also has a `slot` for containing the caption.
  * Put your example todo test to a `array`. Each entry has to be an object with a `name` and `isDone` prop. Make sure some are done.
  * Change the `main.js` so that it uses this array as `data` and renders a `<todo-item>` for each

### 4. After **Step 5**
  * Extract the `<todo-item>` component to a separate file with an `mjs` extension
  * Write a test that checks if the `has-text-success` class is applied properly.
  * Hint: you can access the `HTMLElement` of a Vue `view model` with the `$el` property.

### 5. Stretch goal: emit + listen to events
  * Extend the `<todo-item>` with a checkbox depending on the `isDone` prop.
  * When the checkbox is clicked emit a `todo-changed` event with the updated todo item being the payload.
  * In the parent component update the todo list when this event is caught.

## Tips and tricks

These are not strictly part of the workshop, check them out if care and have the time.

> **TIP #1**: When you come up with classes to use in HTML consider following some conventions. Eg. prefix classes with `js-` that exist *only* so that `JavaScript` code can locate the element(s) they mark. Read more:  https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/

> **TIP #2**: When using a **Content Delivery Network** such as https://cdnjs.com it's strongly advised to protect your users from libraries a hacker tampered with. The [SRI hash](https://www.srihash.org/) allows the browser to detect that someone has secretly altered a package. Surprisingly it's even possible possible to [attack users with CSS](https://hackaday.com/2018/02/25/css-steals-your-web-data/)! Most CDN offer you a `GUI` from inserting `link` tags with the security has filled in. See: https://cdnjs.com/libraries/bulma -> "Copy link tag with SRI".

> **TIP #3**: The Vue.js community puts a lot of effort into their documentations. Check out the following before you put a lot of code in your projects: [Style guide](https://vuejs.org/v2/style-guide/), [Managing app logic](https://skyronic.com/blog/vuex-basics-tutorial), [Language support](http://kazupon.github.io/vue-i18n/), and [Routing](https://router.vuejs.org/).
