const COLORS = ['Red', 'Green', 'Blue'];

function drawColorCollection() {
  COLORS.forEach(addColor);
}

function addColor(color) {
  const parentElement = document.querySelector('.js-color-collection'); // See TIP #1
  const colorElement = document.createElement('li');
  colorElement.innerHTML = color;
  parentElement.appendChild(colorElement);
}
