# No code?

How about you make some. Let's use `npm init` to jumpstart! Here is what happens: when you type `npm init foo` the `Node Package Manager` is going to search for a globally published package called  `create-foo`. If it finds one, then it downloads that package, installs it, runs, finally deletes it. If this package happens to ask a few questions then spews out a bunch of files and folders, then people tend to call it a _"scaffolding tool"_.

```bash
# Install an opinionated scaffolding with npm
npm init @riovir/vue-app
# Press a bunch of enters, then install the project
npm install
# Run the development server with hot code swap
npm run dev

```
