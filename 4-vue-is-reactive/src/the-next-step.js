const template =
`<div class="content" v-if="playedEnough">
  The the point is, that you don't need
  <code>Webpack</code>,
  <code>vue-loader</code>,
  and an enormous <code>node_modules</code> folder
  just to whip up a small Vue.js app.

  {{problemText}}
  <template v-if="problems.length">
    <ul class="has-text-danger">
      <a-problem
        v-for="problem in problems"
        :key="problem"
        :caption="problem"
        @dont-care="ignore" />
    </ul>
  </template>
  <em v-if="!problemText">
    You should totally use this setup.
  </em>
</div>`;

const AProblem = {
  template: `<li>{{caption}} <a @click="dontCare">Don't care.</a></li>`,
  props: {
    caption: { type: String, default: 'A problem' }
  },
  methods: {
    dontCare() {
      this.$emit('dont-care', this.caption);
    }
  }
};

import state from './state.js'

export default {
  template,
  components: { AProblem },
  data: () => ({ state,
    problemText: 'However this setup has some shortcomings:',
    problems: [
      "Doesn't work with IE 11.",
      'There is no place to write tests.',
      'No syntax highlight for your templates.',
      'No linter support.',
      'Generally not very convenient to work with.'
    ]
  }),
  computed: {
    playedEnough() {
      return this.state.timesClicked >= 3;
    }
  },
  methods: {
    ignore(problem) {
      this.problems = this.problems.filter(item => item !== problem);
    },
    startDeletingConclusion() {
      if (!this.problemText) { return; }
      this.problemText = this.problemText.substring(0, this.problemText.length - 1);
      setTimeout(() => this.startDeletingConclusion(), 50);
    }
  },
  watch: {
    problems({ length }) {
      if (!length) {
        this.startDeletingConclusion();
      }
    }
  }
};
