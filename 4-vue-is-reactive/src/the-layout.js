const template =
`<div class="section">
  <div class="container">
    <h1 class="title">Welcome</h1>
    <p class="content">Here is your app back from step 1.</p>
    <slot />
  </div>
</div>`;

export default { template };
