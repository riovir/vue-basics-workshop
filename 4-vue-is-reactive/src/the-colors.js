const template =
`<div class="content">
  <span>
    You still got your colors. Click to
    <button class="button is-primary is-small" @click="addColor('Another color')">
      Add another color
    </button>
  </span>

  <ul>
    <li v-for="(color, index) in colors" :key="index">{{color}}</li>
  </ul>
</div>`;

import state from './state.js';

export default {
  template,
  data: () => state,
  methods: {
    addColor(color) {
      this.colors.push(color);
      this.timesClicked++;
    }
  }
};
