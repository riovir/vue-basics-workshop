import TheLayout from './the-layout.js';
import TheColors from './the-colors.js';
import TheNextStep from './the-next-step.js';

new Vue({
  el: '#app',
  template: `
    <the-layout>
      <the-colors />
      <the-next-step />
    </the-layout>`,
  components: { TheLayout, TheColors, TheNextStep }
});
